import * as bcrypt from 'bcryptjs'

export default class Hash {
  static salt = 10

  /**
   * Encrypts plain string
   * @param str {string}
   * @returns Promise<string> Returns encrypted
   */
  static encrypt(str){
    return bcrypt.hashSync(str, Hash.salt)
  }

  /**
   * Compares encrypted and provided string
   * @param plain {string}
   * @param encrypted {string}
   * @returns Promise<boolean> Returns Boolean if provided string and encrypted string are equal
   */
  static compare(plain, encrypted) {
    return bcrypt.compareSync(plain, encrypted)
  }
}
