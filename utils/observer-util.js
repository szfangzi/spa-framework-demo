const targetMap = new WeakMap()
let activeObserve = null
let timeout = null
const handlers = {
  get(target, key, receiver) {
    const res = Reflect.get(target, key, receiver)
    track(target, key)
    return (typeof res === 'object' && res !== null)
      ? observable(res)
      : res
  },
  set(target, key, value, receiver) {
    const res = Reflect.set(target, key, value, receiver)
    // console.log(target, key, res);
    clearTimeout(timeout)
    timeout = setTimeout(() => {
      trigger(target, key)
    }, 0)
    return res
  }
}

function observable(target) {
  return new Proxy(target, handlers)
}

function track(target, key) {
  if (!activeObserve) return
  let depsMap = targetMap.get(target)
  if (!depsMap) targetMap.set(target, (depsMap = new Map()))
  let deps = depsMap.get(key)
  if (!deps) depsMap.set(key, (deps = new Set()))
  // console.log('deps', !deps, !deps.has(activeObserve), target, key);
  if (!deps.has(activeObserve)) deps.add(activeObserve)
}

function trigger(target, key) {
  let depsMap = targetMap.get(target)
  // console.log(depsMap, 'depsMap');
  if(!(depsMap instanceof Map)) return
  let deps = depsMap.get(key)
  // console.log(deps, 'deps');
  if(!(deps instanceof Set)) return
  // deps.forEach(fn => fn())

  // deps就是observe回调，observe回调里面就是个render(), 所以只要执行最后一个即可，所以代码其实还能优化！！！！
  Array.from(deps)[deps.size - 1]()
}

function observe(fn) {
  activeObserve = fn
  fn()
  // fn中如果有使用observable对象，就会执行会触发proxy handler get，就会走track，像arr[0]这种有可能会触发多次，比如arr.push后，除了值变化，还有length变化，导致fn多次执行，所以通过activeObserve = null关闭track收集依赖
  activeObserve = null
}

export { observe, observable };
