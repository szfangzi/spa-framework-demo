import axios from 'axios'
import { router } from '../router'

const http = axios.create({
  timeout: 5000,
  baseURL: 'https://localhost:8080/api',
  withCredentials: true,
})

http.interceptors.request.use(req => req)
http.interceptors.response.use(res => {
  return res.data
}, err => {
  if (err.response) {
    const { status } = err.response
    if (status === 401) {
      const currRoute = router.getCurrentLocation()
      if (currRoute.url !== 'auth/login')
        router.navigate(`/auth/login?redirect=${currRoute.url}`)
    }
    return Promise.reject(err.response.data)
  }
  return Promise.reject(err)
})

export default http
