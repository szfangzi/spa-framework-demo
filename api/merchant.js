import http from '../utils/http'

export const createMerchant = (data) => {
  return http.post('/merchants', data)
}

export const findMerchant = () => {
  return http.get('/merchants/:id')
}
