import http from '../utils/http'

export const login = (data) => {
  return http.post('/auth/login', data, { withCredentials: true })
}

export const getUserInfo = () => {
  return http.get('/auth/userInfo')
}

export const getTest = () => {
  return http.get('/auth/test')
}
