import { canPushEffect, html, useEffectQueue, useState } from '../core'
import diff from 'virtual-dom/diff'
import patch from 'virtual-dom/patch'
import App from '../App'
import Navigo from 'navigo'
import { coreData, render } from '../core'
import { getUserInfo } from '../api/auth'

const router = new Navigo('/')

const initRouter = () => {
  router.hooks({
    before(done, match) {
      // 切换页面之前允许effect进队列
      canPushEffect.value = true
      // console.log(match.url, 66);
      if (match.url !== 'auth/login') {
        const [user, setUser] = useState('user')
        if (!user.value) {
          getUserInfo().then(userInfo => {
            if (userInfo && userInfo.id) {
              setUser(userInfo)
            } else {
              router.navigate(`/auth/login?redirect=/${match.url}`)
            }
          })
        }
      }
      // 这里可以校验登录
      done()
    },
    after(route) {
      render(route)
      // 页面第一次渲染完毕后执行effect队列
      while (useEffectQueue.length > 0) {
        useEffectQueue.pop()()
      }
      // 页面第一次渲染完毕后关闭effect进队列的状态
      canPushEffect.value = false
    }
  })
  router.on('/auth/login', () => {})
  router.on('/admin/user/list', () => {})
  router.on('/admin/product/list', () => {})
  router.notFound(() => {
    const children = () => html`404`
    let newTree = App({children});
    let patches = diff(coreData.tree, newTree);
    coreData.rootNode = patch(coreData.rootNode, patches);
    coreData.tree = newTree;
  })
  
  // 这个库的要求，必须执行一次
  router.resolve()
}

export {
  router,
  initRouter,
}