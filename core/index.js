import { observable, observe } from '../utils'
import htm from 'htm'
// html函数可以将 html-tagged-template 转 hyperScript
export const html = htm.bind((tagName, properties, ...children) => h(tagName, properties, children))
import App from '../App'
import { diff, patch, h } from 'virtual-dom'
import createElement from 'virtual-dom/create-element'
import { router, initRouter } from '../router'
export const pages = import.meta.globEager('../pages/**')

export const init = (options = {}) => {
  const defaults = {
    container: '#app'
  }
  options = Object.assign(defaults, options)

  if(!document.querySelector(options.container)) throw new Error('容器选择器错误！')
  document.querySelector(options.container).appendChild(coreData.rootNode)

  initRouter()
}

export const canPushEffect = observable({value: false})
const hooks = {}
export const useState = (key, initValue) => {
  if (!hooks[key]) {
    const valueObj = observable({ value: initValue })
    const setState = (value) => {
      valueObj.value = value
    }
    hooks[key] = [valueObj, setState]
    observe(() => {
      valueObj.value
      // if (typeof valueObj.value === 'object') {
      //   for (let k in valueObj.value) {
      //     valueObj.value[k]
      //   }
      // } else {
      //   valueObj.value
      // }
      render()
    })
  }
  return hooks[key]
}


// const memo = {
//   newCounter: {
    
//   }
// }
const memoDeps = new WeakMap()
export const useMemo = (fn, depKeys) => {
  let cache
  // depKeys.forEach(key => {
  //   if (!memos[key]) {
  //     memos[key] = {}
  //   }
  //   memos[key][k] = {
  //     cache,
  //     fn,
  //   }
  // })
  return fn()
}

export const useEffectQueue = []
export const useEffect = (fn) => {
  if (canPushEffect.value) {
    useEffectQueue.push(fn)
  }
}

let tree = App({})

export const coreData = {
  tree,
  rootNode: createElement(tree),
}

export const render = (currRoute) => {
  // console.log('render', currRoute);
  if (!currRoute) currRoute = router.getCurrentLocation()
  let children = null
  for (let k in pages) {
    if (k.includes(currRoute.url)) {
      children = pages[k].default
      break
    }
  }
  if (!children) return

  let layoutList = []
  currRoute.url.split('/').reduce((prev, curr, index) => {
    let layout = '../pages' + prev + '/' + curr + '/_layout.js'
    let hasLayout = !!Object.keys(pages).find(key => key === layout)
    if (hasLayout) {
      layoutList.push(pages[layout].default)
    }
    return prev + '/' + curr
  }, '')
  layoutList.reverse().forEach(layout => {
    children = layout.bind(null, { children })
  })
  // 渲染当前页面组件
  let newTree = App({ children });
  let patches = diff(coreData.tree, newTree);
  coreData.rootNode = patch(coreData.rootNode, patches);
  coreData.tree = newTree;
}