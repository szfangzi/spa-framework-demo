import { getTest } from '../../../api/auth'
import { html, useEffect, useMemo, useState } from '../../../core'

export default function ProductList() {
  const [counter, setCounter] = useState('counter', 0)
  const [counter2, setCounter2] = useState('counter2', 0)
  let x = useMemo(() => {
    // console.log('useMemo');
    const result = counter.value * 2
    return result
  }, [counter.value])

  useEffect(() => {
    getTest().then(res => {
      setCounter2(res)
    })
  })
  
  return html`<div>
    <button onclick=${() => {
      setCounter(counter.value + 1)
    }}>ok</button>
    product list ${x}, ${counter.value}, ${counter2.value}
  </div>`
}