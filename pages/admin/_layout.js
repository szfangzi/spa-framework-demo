import { html, useState } from "../../core";
import { router } from "../../router";

export default function _layout({ children }) {
  const [user ,setUser] = useState('user')
  return html`<div style=${{width: '100vw', height: '100vh', display: 'flex'}}>
    <div style=${{width: '200px', height: '100vh', backgroundColor: '#00a'}}>
      <ul style=${{listStyle: 'none', color: 'white'}}>
        <li onclick=${() => router.navigate('/admin/user/list')}>用户列表</li>
        <li onclick=${() => router.navigate('/admin/product/list')}>商品列表</li>
      </ul>
    </div>
    <div style=${{ flex: 1, height: '100vh', display: 'flex', flexDirection: 'column' }}>
      <div style=${{height: '60px', backgroundColor: '#a00'}}>${user.value ? user.value.nickname : ''}</div>
      <div style=${{height: 'calc(100vh - 60px)', backgroundColor: '#0a0'}}>${children()}</div>
    </div>
  </div>`
}