import { html, useState } from '../../../core'

export default function UserList() {
  const add = () => {
    const [user, setUser] = useState('user')
    // user.value.nickname = user.value.nickname + '1'
    setUser(Object.assign(user, {nickname: user.value.nickname + '1'}))
  }
  return html`<div onclick=${() => add()}>user list</div>`
}