import { login } from '../../api/auth'
import { html, useState } from '../../core'
import { router } from '../../router'

export default function Login() {
  const [username, setUsername] = useState('Login.username', '')
  const [password, setPassword] = useState('Login.password', '')
  const submit = async () => {
    // console.log(username.value, Hash.encrypt(password.value))
    try {
      const [,setUser] = useState('user')
      let user = await login({ username: username.value, password: password.value })
      setUser(user)
      const url = (router.getCurrentLocation().params && router.getCurrentLocation().params.redirect) || '/admin/user/list'
      router.navigate(url)
    } catch (e) {
      console.dir(e);
      alert(e.message)
    }
    
    // console.log(user, 'user');
  }
  return html`<div>
    <input onchange=${(e) => setUsername(e.target.value)} />
    <input onchange=${(e) => setPassword(e.target.value)} type='password' />
    <button onclick=${submit}>登录</button>
  </div>`
}