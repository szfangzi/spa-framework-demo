import { useState } from './core'
import { html } from './core'
import { router } from './router'

export default function App({ children = () => '' }) {
  return html`
    <div>
    ${children()}
    </div>
  `
}